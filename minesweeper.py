import pygame
import random
import time
from button import PlayAgainButton
from button import ReturnMenuButton

pygame.init()
pygame.mixer.init()

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (200, 200, 200)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
YELLOW = (255, 255, 0)

CELL_SIZE = 30
BEGINNER_SIZE = 9
ADVANCED_SIZE = 16

clear_sound = pygame.mixer.Sound("ms-clear.mp3")
flag_sound = pygame.mixer.Sound("ms-flag.mp3")
bomb_sound = pygame.mixer.Sound("ms-bomb.mp3")
victory_sound = pygame.mixer.Sound("ms-victory.mp3")

class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.is_mine = False
        self.is_revealed = False
        self.is_flagged = False
        self.neighbor_mines = 0

class Button:
    def __init__(self, x, y, width, height, text):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.font = pygame.font.Font("arcadeclassic.ttf", 26)

    def draw(self, screen):
        pygame.draw.rect(screen, BLACK, self.rect)
        pygame.draw.rect(screen, YELLOW, self.rect, 2, border_radius=10)
        text_surface = self.font.render(self.text, True, WHITE)
        text_rect = text_surface.get_rect(center=self.rect.center)
        screen.blit(text_surface, text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)

def save_highscore(user_id, game_mode, time, supabase):
    try:
        existing_highscore = supabase.table('highscores').select('score').eq('user_id', user_id).eq('game', game_mode).execute()
        
        if existing_highscore.data:
            current_best = existing_highscore.data[0]['score']
            if time < current_best:
                result = supabase.table('highscores').update({'score': time}).eq('user_id', user_id).eq('game', game_mode).execute()
                print(f"Highscore updated: {result}")
            else:
                print(f"Current time ({time}s) is not better than the existing highscore ({current_best}s)")
        else:
            result = supabase.table('highscores').insert({
                'user_id': user_id,
                'game': game_mode,
                'score': time
            }).execute()
            print(f"New highscore saved: {result}")
    except Exception as e:
        print(f"Error saving highscore: {str(e)}")

class Minesweeper:
    def __init__(self, grid_size, num_mines, game_mode, user_id, supabase):
        self.grid_size = grid_size
        self.num_mines = num_mines
        self.game_mode = game_mode
        self.user_id = user_id
        self.supabase = supabase
        self.width = grid_size * CELL_SIZE
        self.height = grid_size * CELL_SIZE + 80  
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("Minesweeper")
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font("arcadeclassic.ttf", 28)
        self.reset_game()

    def reset_game(self):
        self.grid = [[Cell(x, y) for y in range(self.grid_size)] for x in range(self.grid_size)]
        self.game_over = False
        self.win = False
        self.bombs_left = self.num_mines
        self.place_mines()
        self.calculate_neighbor_mines()
        self.start_time = time.time()
        self.elapsed_time = 0

    def place_mines(self):
        mines_placed = 0
        while mines_placed < self.num_mines:
            x = random.randint(0, self.grid_size - 1)
            y = random.randint(0, self.grid_size - 1)
            if not self.grid[x][y].is_mine:
                self.grid[x][y].is_mine = True
                mines_placed += 1

    def calculate_neighbor_mines(self):
        for x in range(self.grid_size):
            for y in range(self.grid_size):
                if not self.grid[x][y].is_mine:
                    for dx in [-1, 0, 1]:
                        for dy in [-1, 0, 1]:
                            nx, ny = x + dx, y + dy
                            if 0 <= nx < self.grid_size and 0 <= ny < self.grid_size and self.grid[nx][ny].is_mine:
                                self.grid[x][y].neighbor_mines += 1

    def reveal_cell(self, x, y):
        if not (0 <= x < self.grid_size and 0 <= y < self.grid_size):
            return

        cell = self.grid[x][y]
        if cell.is_revealed or cell.is_flagged:
            return

        cell.is_revealed = True

        if cell.is_mine:
            self.game_over = True
            bomb_sound.play()
        else:
            clear_sound.play()
            if cell.neighbor_mines == 0:
                for dx in [-1, 0, 1]:
                    for dy in [-1, 0, 1]:
                        self.reveal_cell(x + dx, y + dy)

        if self.check_win():
            self.win = True
            self.game_over = True
            victory_sound.play()
            save_highscore(self.user_id, self.game_mode, int(self.elapsed_time), self.supabase)

    def toggle_flag(self, x, y):
        if 0 <= x < self.grid_size and 0 <= y < self.grid_size:
            cell = self.grid[x][y]
            if not cell.is_revealed:
                if cell.is_flagged:
                    cell.is_flagged = False
                    self.bombs_left += 1
                else:
                    cell.is_flagged = True
                    self.bombs_left -= 1
                flag_sound.play()

    def check_win(self):
        for x in range(self.grid_size):
            for y in range(self.grid_size):
                cell = self.grid[x][y]
                if not cell.is_mine and not cell.is_revealed:
                    return False
        return True

    def draw_grid(self):
        for x in range(self.grid_size):
            for y in range(self.grid_size):
                cell = self.grid[x][y]
                rect = pygame.Rect(x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE, CELL_SIZE)
                
                if cell.is_revealed:
                    pygame.draw.rect(self.screen, WHITE, rect)
                    if cell.is_mine:
                        pygame.draw.circle(self.screen, BLACK, rect.center, CELL_SIZE // 4)
                    elif cell.neighbor_mines > 0:
                        text = self.font.render(str(cell.neighbor_mines), True, BLACK)
                        self.screen.blit(text, text.get_rect(center=rect.center))
                else:
                    pygame.draw.rect(self.screen, GRAY, rect)
                    if cell.is_flagged:
                        pygame.draw.polygon(self.screen, RED, [
                            (x * CELL_SIZE + 5, y * CELL_SIZE + 5),
                            (x * CELL_SIZE + CELL_SIZE - 5, y * CELL_SIZE + CELL_SIZE // 2),
                            (x * CELL_SIZE + 5, y * CELL_SIZE + CELL_SIZE - 5)
                        ])

                pygame.draw.rect(self.screen, BLACK, rect, 1)

        bombs_left_text = self.font.render(f"Bombe Ramase: {self.bombs_left}", True, YELLOW)
        self.screen.blit(bombs_left_text, (10, self.height - 70))

        timer_text = self.font.render(f"Timp scurs: {int(self.elapsed_time)}s", True, YELLOW)
        self.screen.blit(timer_text, (10, self.height - 35))

    def draw_end_screen(self):
        overlay = pygame.Surface((self.width, self.height))
        overlay.set_alpha(200)
        overlay.fill(BLACK)
        self.screen.blit(overlay, (0, 0))

        message = "You Win!" if self.win else "Game Over!"
        text = self.font.render(message, True, YELLOW)
        self.screen.blit(text, text.get_rect(center=(self.width // 2, self.height // 3)))

        if self.win:
            time_text = self.font.render(f"Timp: {int(self.elapsed_time)}s", True, YELLOW)
            self.screen.blit(time_text, time_text.get_rect(center=(self.width // 2, self.height // 3 + 40)))

        play_again_button = PlayAgainButton(self.width // 2 - 75, self.height // 2, 150, 50)
        main_menu_button = ReturnMenuButton(self.width // 2 - 75, self.height // 2 + 60, 150, 50)

        play_again_button.draw(self.screen)
        main_menu_button.draw(self.screen)

        return play_again_button, main_menu_button

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return "QUIT"
                elif event.type == pygame.MOUSEBUTTONDOWN and not self.game_over:
                    x, y = event.pos[0] // CELL_SIZE, event.pos[1] // CELL_SIZE
                    if event.button == 1: 
                        self.reveal_cell(x, y)
                    elif event.button == 3:  
                        self.toggle_flag(x, y)
                elif event.type == pygame.MOUSEBUTTONDOWN and self.game_over:
                    play_again_button, main_menu_button = self.draw_end_screen()
                    if play_again_button.is_clicked(event.pos):
                        self.reset_game()
                    elif main_menu_button.is_clicked(event.pos):
                        return "MAIN_MENU"

            self.screen.fill(BLACK)
            self.draw_grid()
            
            if not self.game_over:
                self.elapsed_time = time.time() - self.start_time

            if self.game_over:
                self.draw_end_screen()

            pygame.display.flip()
            self.clock.tick(30)

        return "QUIT"

def run_minesweeper(user_id, supabase):
    screen = pygame.display.set_mode((300, 200))
    pygame.display.set_caption("Minesweeper Settings")
    clock = pygame.time.Clock()

    beginner_button = Button(75, 50, 150, 50, "Incepator")
    advanced_button = Button(75, 120, 150, 50, "Avansat")

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if beginner_button.is_clicked(event.pos):
                    game = Minesweeper(BEGINNER_SIZE, 10, "minesweeper_beginner", user_id, supabase)
                    result = game.run()
                    if result == "MAIN_MENU":
                        return result
                    running = False
                elif advanced_button.is_clicked(event.pos):
                    game = Minesweeper(ADVANCED_SIZE, 40, "minesweeper_advanced", user_id, supabase)
                    result = game.run()
                    if result == "MAIN_MENU":
                        return result
                    running = False

        screen.fill(BLACK)
        beginner_button.draw(screen)
        advanced_button.draw(screen)
        pygame.display.flip()
        clock.tick(30)

    return "QUIT"

if __name__ == "__main__":
    run_minesweeper()