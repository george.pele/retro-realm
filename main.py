import pygame
import sys
from supabase import create_client
from config import SUPABASE_URL, SUPABASE_KEY
from snake import run_snake
from minesweeper import run_minesweeper
from tetris import run_tetris
from pong import run_pong
from pacman import run_pacman
from button import LoginButton, RegisterButton

pygame.init()

WIDTH, HEIGHT = 800, 600
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Retro Realm")

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (200, 200, 200)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)


font = pygame.font.Font("arcadeclassic.ttf", 32)
small_font = pygame.font.Font("arcadeclassic.ttf", 24)

supabase = create_client(SUPABASE_URL, SUPABASE_KEY)

background_image = pygame.image.load("main-bg.png")
logo_image = pygame.image.load("rr-logo.png")
logo_image = pygame.transform.scale(logo_image, (200, 100))  

class Button:
    
    def __init__(self, x, y, width, height, text, color, text_color, border_color=None):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.color = color
        self.text_color = text_color
        self.border_color = border_color

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, border_radius=10)
        if self.border_color:
            pygame.draw.rect(surface, self.border_color, self.rect, border_radius=10, width=2)
        text_surface = font.render(self.text, True, self.text_color)
        text_rect = text_surface.get_rect(center=self.rect.center)
        surface.blit(text_surface, text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)

def draw_input_field(surface, rect, text, active):
    color = WHITE if active else GRAY
    pygame.draw.rect(surface, WHITE, rect)
    pygame.draw.rect(surface, color, rect, 2)
    text_surface = font.render(text, True, BLACK)
    surface.blit(text_surface, (rect.x + 5, rect.y + 5))

def auth_page():
    email_input = ""
    password_input = ""
    input_active = "email"
    login_button = LoginButton(200, 450, 180, 50)
    register_button = RegisterButton(420, 450, 180, 50)
    error_message = ""

    email_rect = pygame.Rect(200, 250, 400, 50)
    password_rect = pygame.Rect(200, 350, 400, 50)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if email_rect.collidepoint(event.pos):
                    input_active = "email"
                elif password_rect.collidepoint(event.pos):
                    input_active = "password"
                elif login_button.is_clicked(event.pos):
                    try:
                        user = supabase.auth.sign_in_with_password({"email": email_input, "password": password_input})
                        return user
                    except Exception as e:
                        error_message = f"Login failed: {str(e)}"
                elif register_button.is_clicked(event.pos):
                    try:
                        user = supabase.auth.sign_up({"email": email_input, "password": password_input})
                        return user
                    except Exception as e:
                        error_message = f"Registration failed: {str(e)}"
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_TAB:
                    input_active = "password" if input_active == "email" else "email"
                elif event.key == pygame.K_RETURN:
                    try:
                        user = supabase.auth.sign_in_with_password({"email": email_input, "password": password_input})
                        return user
                    except Exception as e:
                        error_message = f"Login failed: {str(e)}"
                elif event.key == pygame.K_BACKSPACE:
                    if input_active == "email":
                        email_input = email_input[:-1]
                    else:
                        password_input = password_input[:-1]
                else:
                    if input_active == "email":
                        email_input += event.unicode
                    else:
                        password_input += event.unicode

        screen.blit(background_image, (0, 0))
        screen.blit(logo_image, (WIDTH // 2 - logo_image.get_width() // 2, 20))
        
        draw_input_field(screen, email_rect, email_input, input_active == "email")
        draw_input_field(screen, password_rect, "*" * len(password_input), input_active == "password")
        
        email_label = font.render("Email:", True, YELLOW)
        password_label = font.render("Parola:", True, YELLOW)
        
        screen.blit(email_label, (200, 220))
        screen.blit(password_label, (200, 320))
        
        login_button.draw(screen)
        register_button.draw(screen)
        
        if error_message:
            error_surface = font.render(error_message, True, RED)
            screen.blit(error_surface, (200, 520))
        
        pygame.display.flip()

def game_selection_page():
    games = ["Pac-Man", "Minesweeper", "Tetris", "Pong", "Snake"]
    buttons = [Button(250, 200 + i * 70, 300, 50, game, BLACK, YELLOW, YELLOW) for i, game in enumerate(games)]
    logout_button = Button(20, 20, 130, 40, "Logout", RED, YELLOW, YELLOW)
    highscores_button = Button(650, 20, 130, 40, "Scoruri", BLACK, YELLOW, YELLOW)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if logout_button.is_clicked(event.pos):
                    return "LOGOUT"
                if highscores_button.is_clicked(event.pos):
                    return "HIGHSCORES"
                for button in buttons:
                    if button.is_clicked(event.pos):
                        return button.text

        screen.blit(background_image, (0, 0))
        screen.blit(logo_image, (WIDTH // 2 - logo_image.get_width() // 2, 20))
        
        for button in buttons:
            button.draw(screen)
        
        logout_button.draw(screen)
        highscores_button.draw(screen)
        
        title = font.render("Selecteaza Jocul", True, YELLOW)
        screen.blit(title, (WIDTH // 2 - title.get_width() // 2, 160))
        
        pygame.display.flip()

def get_highscores(user_id):
    try:
        result = supabase.table('highscores').select('game', 'score').eq('user_id', user_id).execute()
        highscores = {}
        for row in result.data:
            game = row['game']
            score = row['score']
            if game.startswith('minesweeper'):
                if game not in highscores or score < highscores[game]:
                    highscores[game] = score
            else:
                if game not in highscores or score > highscores[game]:
                    highscores[game] = score
        return highscores
    except Exception as e:
        print(f"Error fetching highscores: {str(e)}")
        return {}

def highscores_page(user_id):
    back_button = Button(20, 20, 100, 40, "Inapoi", BLACK, YELLOW, YELLOW)

    highscores = get_highscores(user_id)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if back_button.is_clicked(event.pos):
                    return

        screen.blit(background_image, (0, 0))
        screen.blit(logo_image, (WIDTH // 2 - logo_image.get_width() // 2, 20))
        
        back_button.draw(screen)
        
        title = font.render("Scoruri Record", True, YELLOW)
        screen.blit(title, (WIDTH // 2 - title.get_width() // 2, 160))
        
        y_offset = 220
        for game, score in highscores.items():
            game_name = game.replace('_', ' ').title()
            if game.startswith('minesweeper'):
                score_text = f"{score} seconds"
            else:
                score_text = str(score)
            game_text = small_font.render(f"{game_name}: {score_text}", True, YELLOW)
            screen.blit(game_text, (WIDTH // 2 - game_text.get_width() // 2, y_offset))
            y_offset += 40
        
        pygame.display.flip()

def main():
    user = None
    while True:
        if not user:
            user = auth_page()
        if user:
            supabase.auth.set_session(user.session.access_token, user.session.refresh_token)
            
            print(f"User ID: {user.user.id}")
            print(f"Access Token: {user.session.access_token[:10]}...")
            print(f"Refresh Token: {user.session.refresh_token[:10]}...")
            
            selected_game = game_selection_page()
            if selected_game == "LOGOUT":
                user = None
                continue
            elif selected_game == "HIGHSCORES":
                highscores_page(user.user.id)
                continue
            print(f"Selected game: {selected_game}")
            
            if selected_game == "Snake":
                run_snake(user.user.id, supabase)
            elif selected_game == "Minesweeper":
                run_minesweeper(user.user.id, supabase)
            elif selected_game == "Tetris":
                run_tetris(user.user.id, supabase)
            elif selected_game == "Pong":
                run_pong()
            elif selected_game == "Pac-Man":
                run_pacman(user.user.id, supabase)
            else:
                print(f"{selected_game} is not implemented yet.")
                pygame.time.wait(2000)

            screen = pygame.display.set_mode((WIDTH, HEIGHT))
            pygame.display.set_caption("Retro Realm")

if __name__ == "__main__":
    main()
