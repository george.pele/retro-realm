import pygame
import random
import os
from button import PlayAgainButton
from button import ReturnMenuButton

pygame.init()
pygame.mixer.init()

BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)

WALL_SIZE = 30
PLAYER_SIZE = 30
DOT_SIZE = 6
GHOST_SIZE = 30
WIDTH = 570
HEIGHT = 630
SPEED = 5

screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Pac-Man")

player_img = pygame.image.load("pac-player.png")
player_img = pygame.transform.scale(player_img, (PLAYER_SIZE, PLAYER_SIZE))
ghost_img = pygame.image.load("pac-ghost.png")
ghost_img = pygame.transform.scale(ghost_img, (GHOST_SIZE, GHOST_SIZE))
bonus_dot_img = pygame.image.load("pac-bonus.png")
bonus_dot_img = pygame.transform.scale(bonus_dot_img, (WALL_SIZE, WALL_SIZE))

background_sound = pygame.mixer.Sound("pac-bg.mp3")
level_up_sound = pygame.mixer.Sound("pac-lvlup.mp3")
game_over_sound = pygame.mixer.Sound("pac-gameover.mp3")

maze = [
    "WWWWWWWWWWWWWWWWWWW",
    "W.........W.......W",
    "W.WWW.WWW.W.WWW.W.W",
    "W.............W.W.W",
    "W.WWW.W.WWW.WWW.W.W",
    "W.....W...W.......W",
    "WWWWW.WWW W.WWWWW.W",
    "WWWWW.W   W.WWWWW.W",
    "WWWWW.W WWW.WWWWW.W",
    "     .  W  .      W",
    "WWWWW.W WWW.WWWWW.W",
    "WWWWW.W   W.WWWWW.W",
    "WWWWW.W WWW.WWWWW.W",
    "W.........W.......W",
    "W.WWW.WWW.W.WWW.W.W",
    "W...W.....P.....W.W",
    "WWW.W.W.WWW.W.W.W.W",
    "W.....W...W.W.....W",
    "W.WWWWWWW.W.WWWWW.W",
    "W.................W",
    "WWWWWWWWWWWWWWWWWWW"
]

class Player:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.direction = (0, 0)
        self.original_image = player_img
        self.image = self.original_image

    def move(self):
        new_x = self.x + self.direction[0] * SPEED
        new_y = self.y + self.direction[1] * SPEED
        if not is_collision(new_x, new_y):
            self.x = new_x
            self.y = new_y

    def draw(self):
        if self.direction == (-1, 0):  
            self.image = pygame.transform.flip(self.original_image, True, False)
        elif self.direction == (1, 0): 
            self.image = self.original_image
        elif self.direction == (0, -1): 
            self.image = pygame.transform.rotate(self.original_image, 90)
        elif self.direction == (0, 1):  
            self.image = pygame.transform.rotate(self.original_image, -90)
        
        screen.blit(self.image, (self.x - PLAYER_SIZE // 2, self.y - PLAYER_SIZE // 2))

class Ghost:
    def __init__(self, x, y, speed):
        self.x = x
        self.y = y
        self.speed = speed
        self.direction = random.choice([(0, -1), (0, 1), (-1, 0), (1, 0)])

    def move(self):
        new_x = self.x + self.direction[0] * self.speed
        new_y = self.y + self.direction[1] * self.speed
        if is_collision(new_x, new_y):
            self.direction = random.choice([(0, -1), (0, 1), (-1, 0), (1, 0)])
        else:
            self.x = new_x
            self.y = new_y

    def draw(self):
        if ghost_img:
            screen.blit(ghost_img, (self.x - GHOST_SIZE // 2, self.y - GHOST_SIZE // 2))
        else:
            pygame.draw.circle(screen, RED, (self.x, self.y), GHOST_SIZE // 2)

class BonusDot:
    def __init__(self):
        self.spawn()

    def spawn(self):
        while True:
            x = random.randint(1, len(maze[0]) - 2)
            y = random.randint(1, len(maze) - 2)
            if maze[y][x] == ' ':
                self.x = x * WALL_SIZE + WALL_SIZE // 2
                self.y = y * WALL_SIZE + WALL_SIZE // 2
                self.spawn_time = pygame.time.get_ticks()
                break

    def draw(self):
        screen.blit(bonus_dot_img, (self.x - WALL_SIZE // 2, self.y - WALL_SIZE // 2))

    def should_despawn(self):
        return pygame.time.get_ticks() - self.spawn_time > 10000  

def is_collision(x, y):
    grid_x = int(x // WALL_SIZE)
    grid_y = int(y // WALL_SIZE)
    return maze[grid_y][grid_x] == 'W'

def draw_maze():
    for y, row in enumerate(maze):
        for x, cell in enumerate(row):
            if cell == 'W':
                pygame.draw.rect(screen, BLUE, (x * WALL_SIZE, y * WALL_SIZE, WALL_SIZE, WALL_SIZE))

def save_highscore(user_id, score, supabase):
    try:
        result = supabase.table('highscores').insert({
            'user_id': user_id,
            'game': 'pacman',
            'score': score
        }).execute()
        print(f"Highscore saved: {result}")
    except Exception as e:
        print(f"Error saving highscore: {str(e)}")

def draw_end_screen(score, user_id, supabase):
    screen.fill(BLACK)
    font = pygame.font.Font("arcadeclassic.ttf", 58)
    text = font.render("Game Over", True, YELLOW)
    screen.blit(text, (WIDTH // 2 - text.get_width() // 2, HEIGHT // 2 - text.get_height() // 2 - 100))

    score_text = font.render(f"Scor: {score}", True, YELLOW)
    screen.blit(score_text, (WIDTH // 2 - score_text.get_width() // 2, HEIGHT // 2 - score_text.get_height() // 2))

    play_again_button = PlayAgainButton(WIDTH // 2 - 150, HEIGHT // 2 + 50, 300, 50)
    main_menu_button = ReturnMenuButton(WIDTH // 2 - 150, HEIGHT // 2 + 120, 300, 50)

    play_again_button.draw(screen)
    main_menu_button.draw(screen)

    pygame.display.flip()

    save_highscore(user_id, score, supabase)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return "quit"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if play_again_button.is_clicked(event.pos):
                    return "play_again"
                elif main_menu_button.is_clicked(event.pos):
                    return "main_menu"

class Button:
    def __init__(self, x, y, width, height, text, color, text_color):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.color = color
        self.text_color = text_color
        self.font = pygame.font.Font("arcadeclassic.ttf", 28)

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect)
        text_surface = self.font.render(self.text, True, self.text_color)
        text_rect = text_surface.get_rect(center=self.rect.center)
        surface.blit(text_surface, text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)

def run_pacman(user_id, supabase):
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Pac-Man")
    clock = pygame.time.Clock()
    level = 1
    score = 0
    font = pygame.font.Font("arcadeclassic.ttf", 28)

    if background_sound:
        background_sound.play(-1) 

    while True:
        player = None
        ghosts = []
        dots = []
        bonus_dot = None
        last_bonus_spawn = 0

        for y, row in enumerate(maze):
            for x, cell in enumerate(row):
                if cell == 'P':
                    player = Player(x * WALL_SIZE + WALL_SIZE // 2, y * WALL_SIZE + WALL_SIZE // 2)
                elif cell == '.':
                    dots.append((x * WALL_SIZE + WALL_SIZE // 2, y * WALL_SIZE + WALL_SIZE // 2))

        ghost_speed = SPEED // 2 + (level - 1)
        num_ghosts = min(4 + level - 1, 15)  
        for _ in range(num_ghosts):
            while True:
                x = random.randint(1, len(maze[0]) - 2)
                y = random.randint(1, len(maze) - 2)
                if maze[y][x] == ' ':
                    ghosts.append(Ghost(x * WALL_SIZE + WALL_SIZE // 2, y * WALL_SIZE + WALL_SIZE // 2, ghost_speed))
                    break

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        player.direction = (-1, 0)
                    elif event.key == pygame.K_RIGHT:
                        player.direction = (1, 0)
                    elif event.key == pygame.K_UP:
                        player.direction = (0, -1)
                    elif event.key == pygame.K_DOWN:
                        player.direction = (0, 1)

            screen.fill(BLACK)
            draw_maze()

            player.move()
            player.draw()

            for ghost in ghosts:
                ghost.move()
                ghost.draw()

            for dot in dots[:]:
                if ((player.x - dot[0])**2 + (player.y - dot[1])**2)**0.5 < PLAYER_SIZE // 2 + DOT_SIZE:
                    dots.remove(dot)
                    score += 10

            for dot in dots:
                pygame.draw.circle(screen, WHITE, dot, DOT_SIZE)

            current_time = pygame.time.get_ticks()
            if bonus_dot is None and current_time - last_bonus_spawn > 20000: 
                bonus_dot = BonusDot()
                last_bonus_spawn = current_time
            elif bonus_dot:
                if bonus_dot.should_despawn():
                    bonus_dot = None
                else:
                    bonus_dot.draw()
                    if ((player.x - bonus_dot.x)**2 + (player.y - bonus_dot.y)**2)**0.5 < PLAYER_SIZE // 2 + WALL_SIZE // 2:
                        score += 50
                        bonus_dot = None

            score_text = font.render(f"Scor: {score}", True, WHITE)
            level_text = font.render(f"Nivel: {level}", True, WHITE)
            screen.blit(score_text, (WIDTH - score_text.get_width() - 10, 10))
            screen.blit(level_text, (WIDTH - level_text.get_width() - 10, 50))

            for ghost in ghosts:
                if ((player.x - ghost.x)**2 + (player.y - ghost.y)**2)**0.5 < PLAYER_SIZE // 2 + GHOST_SIZE // 2:
                    running = False
                    if background_sound:
                        background_sound.stop()
                    if game_over_sound:
                        game_over_sound.play()
                    result = draw_end_screen(score, user_id, supabase)
                    if result == "play_again":
                        level = 1
                        score = 0
                        if background_sound:
                            background_sound.play(-1)
                        break
                    elif result == "main_menu" or result == "quit":
                        return

            if not dots:
                level += 1
                if level_up_sound:
                    level_up_sound.play()
                break 

            pygame.display.flip()
            clock.tick(30)

if __name__ == "__main__":
    run_pacman()