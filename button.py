import pygame

class PlayAgainButton:
    def __init__(self, x, y, width, height):
        self.rect = pygame.Rect(x, y, width, height)
        self.color = (0, 0, 0)  
        self.text_color = (255, 255, 255) 
        self.font = pygame.font.Font("arcadeclassic.ttf", 24)
        self.text = self.font.render("RESTART", True, self.text_color)
        self.text_rect = self.text.get_rect(center=self.rect.center)

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, border_radius=10)
        pygame.draw.rect(surface, (255, 255, 0), self.rect, 2, border_radius=10)
        surface.blit(self.text, self.text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)
    

class ReturnMenuButton:
    def __init__(self, x, y, width, height):
        self.rect = pygame.Rect(x, y, width, height)
        self.color = (0, 0, 0) 
        self.text_color = (255, 255, 255) 
        self.font = pygame.font.Font("arcadeclassic.ttf", 24)
        self.text = self.font.render("MENIU", True, self.text_color)
        self.text_rect = self.text.get_rect(center=self.rect.center)

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, border_radius=10)
        pygame.draw.rect(surface, (255, 255, 0), self.rect, 2, border_radius=10) 
        surface.blit(self.text, self.text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)


class LoginButton:
    def __init__(self, x, y, width, height):
        self.rect = pygame.Rect(x, y, width, height)
        self.color = (0, 0, 0) 
        self.text_color = (255, 255, 0) 
        self.font = pygame.font.Font("arcadeclassic.ttf", 30)
        self.text = self.font.render("Conectare", True, self.text_color)
        self.text_rect = self.text.get_rect(center=self.rect.center)

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, border_radius=10)
        pygame.draw.rect(surface, (255, 255, 0), self.rect, 2, border_radius=10)  
        surface.blit(self.text, self.text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)        
    
class RegisterButton:
    def __init__(self, x, y, width, height):
        self.rect = pygame.Rect(x, y, width, height)
        self.color = (0, 0, 0) 
        self.text_color = (255, 255, 0) 
        self.font = pygame.font.Font("arcadeclassic.ttf", 30)
        self.text = self.font.render("Inregistrare", True, self.text_color)
        self.text_rect = self.text.get_rect(center=self.rect.center)

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, border_radius=10)
        pygame.draw.rect(surface, (255, 255, 0), self.rect, 2, border_radius=10)  
        surface.blit(self.text, self.text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)