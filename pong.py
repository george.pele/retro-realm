import pygame
import random
import time
import sys
from button import PlayAgainButton
from button import ReturnMenuButton

pygame.init()
pygame.mixer.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

WIDTH = 800
HEIGHT = 600
BALL_SIZE = 30
PADDLE_WIDTH = 15
PADDLE_HEIGHT = 90

background_img = pygame.image.load("pong-bg.png")
background_img = pygame.transform.scale(background_img, (WIDTH, HEIGHT))

paddle_img = pygame.image.load("pong-paddle.png")
paddle_img = pygame.transform.scale(paddle_img, (PADDLE_WIDTH, PADDLE_HEIGHT))

ball_img = pygame.image.load("pong-ball.png")
ball_img = pygame.transform.scale(ball_img, (BALL_SIZE, BALL_SIZE))

bg_music = pygame.mixer.Sound("pong-bg.mp3")
paddle_hit_sound = pygame.mixer.Sound("pong-paddle.mp3")
gameover_sound = pygame.mixer.Sound("pong-gameover.mp3")

class Paddle:
    def __init__(self, x, y):
        self.rect = pygame.Rect(x, y, PADDLE_WIDTH, PADDLE_HEIGHT)
        self.initial_speed = 5
        self.speed = self.initial_speed

    def move_up(self):
        if self.rect.top > 0:
            self.rect.y -= self.speed

    def move_down(self):
        if self.rect.bottom < HEIGHT:
            self.rect.y += self.speed

    def draw(self, screen):
        screen.blit(paddle_img, self.rect)

    def increase_speed(self):
        self.speed += 0.2

    def reset_speed(self):
        self.speed = self.initial_speed

class Ball:
    def __init__(self):
        self.rect = pygame.Rect(WIDTH // 2 - BALL_SIZE // 2, HEIGHT // 2 - BALL_SIZE // 2, BALL_SIZE, BALL_SIZE)
        self.initial_speed = 4
        self.speed = self.initial_speed
        self.dx = random.choice([-self.speed, self.speed])
        self.dy = random.choice([-self.speed, self.speed])

    def move(self):
        self.rect.x += self.dx
        self.rect.y += self.dy

        if self.rect.top <= 0:
            self.rect.top = 0
            self.dy = abs(self.dy)
        elif self.rect.bottom >= HEIGHT:
            self.rect.bottom = HEIGHT
            self.dy = -abs(self.dy)

    def reset(self):
        self.rect.center = (WIDTH // 2, HEIGHT // 2)
        self.speed = self.initial_speed
        self.dx = random.choice([-self.speed, self.speed])
        self.dy = random.choice([-self.speed, self.speed])

    def draw(self, screen):
        screen.blit(ball_img, self.rect)

    def increase_speed(self):
        self.speed += 0.5
        self.dx = self.speed if self.dx > 0 else -self.speed
        self.dy = self.speed if self.dy > 0 else -self.speed

class Button:
    def __init__(self, x, y, width, height, text, color, text_color):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.color = color
        self.text_color = text_color
        self.font = pygame.font.Font("arcadeclassic.ttf", 26)

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)
        text_surface = self.font.render(self.text, True, self.text_color)
        text_rect = text_surface.get_rect(center=self.rect.center)
        screen.blit(text_surface, text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)

class Pong:
    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption("Pong")
        self.clock = pygame.time.Clock()

        self.paddle1 = Paddle(50, HEIGHT // 2 - PADDLE_HEIGHT // 2)
        self.paddle2 = Paddle(WIDTH - 50 - PADDLE_WIDTH, HEIGHT // 2 - PADDLE_HEIGHT // 2)
        self.ball = Ball()

        self.score1 = 0
        self.score2 = 0

        self.font = pygame.font.Font("arcadeclassic.ttf", 28)
        self.game_over = False
        self.paused = False
        self.start_time = time.time()
        self.last_speed_increase = time.time()
        self.pause_start_time = 0

        self.play_again_button = PlayAgainButton(WIDTH // 4, HEIGHT // 2, 200, 50)
        self.main_menu_button = ReturnMenuButton(WIDTH // 4 * 3 - 200, HEIGHT // 2, 200, 50)

        bg_music.play(-1)  

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                self.toggle_pause()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.game_over or self.paused:
                    if self.play_again_button.is_clicked(event.pos):
                        self.reset_game()
                    elif self.main_menu_button.is_clicked(event.pos):
                        bg_music.stop()
                        return "MAIN_MENU"

        if not self.game_over and not self.paused:
            keys = pygame.key.get_pressed()
            if keys[pygame.K_w]:
                self.paddle1.move_up()
            if keys[pygame.K_s]:
                self.paddle1.move_down()
            if keys[pygame.K_UP]:
                self.paddle2.move_up()
            if keys[pygame.K_DOWN]:
                self.paddle2.move_down()

        return True

    def update(self):
        if self.game_over or self.paused:
            return

        self.ball.move()

        if self.ball.rect.left <= 0:
            self.score2 += 1
            self.ball.reset()
        elif self.ball.rect.right >= WIDTH:
            self.score1 += 1
            self.ball.reset()

        if self.ball.rect.colliderect(self.paddle1.rect):
            self.ball.rect.left = self.paddle1.rect.right
            self.ball.dx = abs(self.ball.dx)
            paddle_hit_sound.play()
        elif self.ball.rect.colliderect(self.paddle2.rect):
            self.ball.rect.right = self.paddle2.rect.left
            self.ball.dx = -abs(self.ball.dx)
            paddle_hit_sound.play()

        current_time = time.time()
        if current_time - self.last_speed_increase >= 7:
            self.ball.increase_speed()
            self.paddle1.increase_speed()
            self.paddle2.increase_speed()
            self.last_speed_increase = current_time

        time_elapsed = current_time - self.start_time
        if self.score1 >= 11 or self.score2 >= 11 or time_elapsed >= 60:
            self.game_over = True
            bg_music.stop()
            gameover_sound.play()

    def draw(self):
        self.screen.blit(background_img, (0, 0))
        self.paddle1.draw(self.screen)
        self.paddle2.draw(self.screen)
        self.ball.draw(self.screen)

        score_text = self.font.render(f"{self.score1} - {self.score2}", True, YELLOW)
        self.screen.blit(score_text, (WIDTH // 2 - score_text.get_width() // 2, 20))

        time_elapsed = time.time() - self.start_time
        if self.paused:
            time_elapsed -= time.time() - self.pause_start_time
        time_left = max(0, 60 - int(time_elapsed))
        time_text = self.font.render(f"Timp: {time_left}", True, YELLOW)
        self.screen.blit(time_text, (WIDTH - time_text.get_width() - 20, 20))

        if self.game_over:
            self.draw_end_screen("Game Over!")
        elif self.paused:
            self.draw_end_screen("Paused")

        pygame.display.flip()

    def draw_end_screen(self, message):
        overlay = pygame.Surface((WIDTH, HEIGHT), pygame.SRCALPHA)
        overlay.fill((0, 0, 0, 128))
        self.screen.blit(overlay, (0, 0))

        message_text = self.font.render(message, True, YELLOW)
        self.screen.blit(message_text, (WIDTH // 2 - message_text.get_width() // 2, HEIGHT // 3))
        
        if self.game_over:
            winner_text = self.font.render(
                f"Castigator: Player {'1' if self.score1 > self.score2 else '2' if self.score2 > self.score1 else 'Egal'}", 
                True, 
                YELLOW
            )
            self.screen.blit(winner_text, (WIDTH // 2 - winner_text.get_width() // 2, HEIGHT // 3 + 50))

        self.play_again_button.draw(self.screen)
        self.main_menu_button.draw(self.screen)

    def reset_game(self):
        self.score1 = 0
        self.score2 = 0
        self.ball.reset()
        self.paddle1.reset_speed()
        self.paddle2.reset_speed()
        self.paddle1.rect.y = HEIGHT // 2 - PADDLE_HEIGHT // 2
        self.paddle2.rect.y = HEIGHT // 2 - PADDLE_HEIGHT // 2
        self.game_over = False
        self.paused = False
        self.start_time = time.time()
        self.last_speed_increase = time.time()
        self.pause_start_time = 0
        bg_music.play(-1)

    def toggle_pause(self):
        self.paused = not self.paused
        if self.paused:
            self.pause_start_time = time.time()
            bg_music.stop()
        else:
            self.start_time += time.time() - self.pause_start_time
            self.last_speed_increase += time.time() - self.pause_start_time
            bg_music.play(-1)

def run_pong():
    game = Pong()
    running = True

    while running:
        result = game.handle_events()
        if result == "MAIN_MENU":
            return "MAIN_MENU"
        elif result == False:
            running = False

        game.update()
        game.draw()
        game.clock.tick(60)

    pygame.quit()

if __name__ == "__main__":
    run_pong()