import pygame
import random
from button import PlayAgainButton
from button import ReturnMenuButton
import pygame.mixer

pygame.init()
pygame.mixer.init()


BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
CYAN = (0, 255, 255)
YELLOW = (255, 255, 0)
MAGENTA = (255, 0, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ORANGE = (255, 165, 0)
GRAY = (128, 128, 128)

GRID_WIDTH = 10
GRID_HEIGHT = 20
GRID_SIZE = 30
GAME_WIDTH = GRID_WIDTH * GRID_SIZE
GAME_HEIGHT = GRID_HEIGHT * GRID_SIZE
SIDEBAR_WIDTH = 200
WIDTH = GAME_WIDTH + SIDEBAR_WIDTH
HEIGHT = GAME_HEIGHT

SHAPES = [
    [[1, 1, 1, 1]],
    [[1, 1], [1, 1]],
    [[1, 1, 1], [0, 1, 0]],
    [[1, 1, 1], [1, 0, 0]],
    [[1, 1, 1], [0, 0, 1]],
    [[1, 1, 0], [0, 1, 1]],
    [[0, 1, 1], [1, 1, 0]]
]

COLORS = [CYAN, YELLOW, MAGENTA, RED, GREEN, BLUE, ORANGE]

class Tetromino:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.shape = random.choice(SHAPES)
        self.color = random.choice(COLORS)
        self.rotation = 0

    def rotate(self, direction):
        if direction == 1:  
            self.shape = [list(row) for row in zip(*self.shape[::-1])]
        elif direction == -1:  
            self.shape = [list(row) for row in zip(*self.shape)][::-1]

    def get_shape(self):
        return self.shape


class Tetris:
    def __init__(self):
        self.grid = [[0 for _ in range(GRID_WIDTH)] for _ in range(GRID_HEIGHT)]
        self.current_piece = self.new_piece()
        self.next_piece = self.new_piece()
        self.game_over = False
        self.score = 0
        self.level = 0
        self.lines_cleared = 0

    def new_piece(self):
        return Tetromino(GRID_WIDTH // 2 - 1, 0)

    def valid_move(self, piece, x, y, rotation):
        rotated_shape = piece.shape
        if rotation == 1:
            rotated_shape = [list(row) for row in zip(*rotated_shape[::-1])]
        elif rotation == -1:
            rotated_shape = [list(row) for row in zip(*rotated_shape)][::-1]

        for i, row in enumerate(rotated_shape):
            for j, cell in enumerate(row):
                if cell:
                    if (x + j < 0 or x + j >= GRID_WIDTH or
                        y + i >= GRID_HEIGHT or
                        (y + i >= 0 and self.grid[y + i][x + j])):
                        return False
        return True

    def clear_lines(self):
        lines_to_clear = [i for i in range(GRID_HEIGHT) if all(self.grid[i])]
        lines_cleared = len(lines_to_clear)
        
        if lines_cleared > 0:
            for line in reversed(lines_to_clear):
                del self.grid[line]
            for _ in range(lines_cleared):
                self.grid.insert(0, [0 for _ in range(GRID_WIDTH)])
        
        old_level = self.level
        self.lines_cleared += lines_cleared
        if lines_cleared == 4:
            self.score += 800  
        else:
            self.score += lines_cleared ** 2 * 100
        self.level = self.lines_cleared // 10
        
        return lines_cleared, self.level > old_level

    def place_piece(self, piece):
        for i, row in enumerate(piece.get_shape()):
            for j, cell in enumerate(row):
                if cell:
                    self.grid[piece.y + i][piece.x + j] = piece.color

        lines_cleared, level_up = self.clear_lines()
        self.current_piece = self.next_piece
        self.next_piece = self.new_piece()
        if not self.valid_move(self.current_piece, self.current_piece.x, self.current_piece.y, 0):
            self.game_over = True
        return lines_cleared, level_up

    def move(self, dx, dy):
        if self.valid_move(self.current_piece, self.current_piece.x + dx, self.current_piece.y + dy, 0):
            self.current_piece.x += dx
            self.current_piece.y += dy
            return True
        return False

    def rotate_piece(self, direction):
        if self.valid_move(self.current_piece, self.current_piece.x, self.current_piece.y, direction):
            self.current_piece.rotate(direction)

    def hard_drop(self):
        while self.move(0, 1):
            pass
        return self.place_piece(self.current_piece)

def draw_grid(screen, game):
    for y, row in enumerate(game.grid):
        for x, color in enumerate(row):
            if color:
                pygame.draw.rect(screen, color, (x * GRID_SIZE, y * GRID_SIZE, GRID_SIZE - 1, GRID_SIZE - 1))

    shape = game.current_piece.get_shape()
    for i, row in enumerate(shape):
        for j, cell in enumerate(row):
            if cell:
                pygame.draw.rect(screen, game.current_piece.color,
                                 ((game.current_piece.x + j) * GRID_SIZE,
                                  (game.current_piece.y + i) * GRID_SIZE,
                                  GRID_SIZE - 1, GRID_SIZE - 1))

def draw_next_piece(screen, game):
    font = pygame.font.Font(None, 30)
    next_text = font.render("Next:", True, YELLOW)
    screen.blit(next_text, (GAME_WIDTH + 20, 20))

    shape = game.next_piece.get_shape()
    for i, row in enumerate(shape):
        for j, cell in enumerate(row):
            if cell:
                pygame.draw.rect(screen, game.next_piece.color,
                                 (GAME_WIDTH + 20 + j * 30,
                                  60 + i * 30,
                                  29, 29))

def draw_sidebar(screen, game):
    pygame.draw.rect(screen, GRAY, (GAME_WIDTH, 0, SIDEBAR_WIDTH, HEIGHT))
    
    font = pygame.font.Font(None, 36)
    score_text = font.render(f"Scor: {game.score}", True, YELLOW)
    level_text = font.render(f"Nivel: {game.level}", True, YELLOW)
    screen.blit(score_text, (GAME_WIDTH + 20, HEIGHT - 100))
    screen.blit(level_text, (GAME_WIDTH + 20, HEIGHT - 50))

    draw_next_piece(screen, game)

def draw_button(screen, text, x, y, width, height):
    button_rect = pygame.Rect(x, y, width, height)
    pygame.draw.rect(screen, BLACK, button_rect)
    pygame.draw.rect(screen, YELLOW, button_rect, 2, border_radius=10)
    
    font = pygame.font.Font(None, 36)
    text_surface = font.render(text, True, WHITE)
    text_rect = text_surface.get_rect(center=button_rect.center)
    screen.blit(text_surface, text_rect)

    return button_rect

def save_highscore(user_id, score, supabase):
    try:
        result = supabase.table('highscores').insert({
            'user_id': user_id,
            'game': 'tetris',
            'score': score
        }).execute()
        print(f"Highscore saved: {result}")
    except Exception as e:
        print(f"Error saving highscore: {str(e)}")

def show_game_over_screen(screen, score, user_id, supabase):
    screen.fill(BLACK)
    font = pygame.font.Font(None, 48)
    game_over_text = font.render(f"Game Over! Score: {score}", True, YELLOW)
    screen.blit(game_over_text, (WIDTH // 2 - game_over_text.get_width() // 2, HEIGHT // 2 - 100))

    play_again_button = draw_button(screen, "Play Again", WIDTH // 2 - 100, HEIGHT // 2, 200, 50)
    main_menu_button = draw_button(screen, "Main Menu", WIDTH // 2 - 100, HEIGHT // 2 + 70, 200, 50)

    pygame.display.flip()

    save_highscore(user_id, score, supabase)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if play_again_button.collidepoint(event.pos):
                    return "PLAY_AGAIN"
                elif main_menu_button.collidepoint(event.pos):
                    return "MAIN_MENU"

def run_tetris(user_id, supabase):
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Tetris")
    clock = pygame.time.Clock()

    bg_music = pygame.mixer.Sound("tetris-bg.mp3")
    clear_sound = pygame.mixer.Sound("tetris-clear.mp3")
    level_up_sound = pygame.mixer.Sound("tetris-lvlup.mp3")
    drop_sound = pygame.mixer.Sound("tetris-drop.mp3")

    bg_music.play(-1)  

    while True:
        game = Tetris()
        fall_time = 0
        fall_speed = 500 
        
        running = True
        while running:
            fall_time += clock.get_rawtime()
            clock.tick()

            current_speed = max(50, fall_speed - (game.level * 50))  

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.mixer.stop()  
                    return
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        game.move(-1, 0)
                    elif event.key == pygame.K_RIGHT:
                        game.move(1, 0)
                    elif event.key == pygame.K_DOWN:
                        game.move(0, 1)
                    elif event.key == pygame.K_x:
                        game.rotate_piece(1)  
                    elif event.key == pygame.K_z:
                        game.rotate_piece(-1) 
                    elif event.key == pygame.K_SPACE:
                        lines_cleared, level_up = game.hard_drop()
                        drop_sound.play()
                        if lines_cleared > 0:
                            clear_sound.play()
                        if level_up:
                            level_up_sound.play()

            if fall_time > current_speed:
                fall_time = 0
                if not game.move(0, 1):
                    lines_cleared, level_up = game.place_piece(game.current_piece)
                    drop_sound.play()
                    if lines_cleared > 0:
                        clear_sound.play()
                    if level_up:
                        level_up_sound.play()
                    if game.game_over:
                        pygame.mixer.stop()  
                        result = show_game_over_screen(screen, game.score, user_id, supabase)
                        if result == "QUIT":
                            return
                        elif result == "MAIN_MENU":
                            return
                        else:  
                            bg_music.play(-1)  
                            break

            screen.fill(BLACK)
            draw_grid(screen, game)
            draw_sidebar(screen, game)

            pygame.display.flip()


if __name__ == "__main__":
    run_tetris() 
    pygame.quit()