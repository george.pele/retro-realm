import pygame
import random
import os
import math
from button import PlayAgainButton
from button import ReturnMenuButton

pygame.init()
pygame.mixer.init()


WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
SNAKE_GREEN = (107, 142, 35)
SNAKE_DARK_GREEN = (85, 107, 47)
GRAY = (200, 200, 200)
YELLOW = (255, 255, 0)

WIDTH = 800
HEIGHT = 600
GRID_SIZE = 25
GRID_WIDTH = WIDTH // GRID_SIZE
GRID_HEIGHT = HEIGHT // GRID_SIZE


UP = (0, -1)
DOWN = (0, 1)
LEFT = (-1, 0)
RIGHT = (1, 0)

eat_sound = pygame.mixer.Sound("snake-eat.mp3")
gameover_sound = pygame.mixer.Sound("snake-gameover.mp3")
bg_music = pygame.mixer.Sound("snake-bg.mp3")

food_img = pygame.image.load("snake-food.png")
food_img = pygame.transform.scale(food_img, (GRID_SIZE, GRID_SIZE))
bonus_food_img = pygame.image.load("snake-bonus.png")
bonus_food_img = pygame.transform.scale(bonus_food_img, (GRID_SIZE, GRID_SIZE))

WALL_POSITIONS = (
    [(x, 0) for x in range(GRID_WIDTH)] +
    [(x, GRID_HEIGHT - 1) for x in range(GRID_WIDTH)] +
    [(0, y) for y in range(GRID_HEIGHT)] +
    [(GRID_WIDTH - 1, y) for y in range(GRID_HEIGHT)] +
    [(5, 5), (6, 5), (7, 5)] +
    [(20, 15), (20, 16), (20, 17), (20, 18), (20, 19)] +
    [(10, 10), (11, 10), (12, 10), (13, 10), (14, 10)]
)

class Button:
    def __init__(self, x, y, width, height, text, color, text_color, border_color=YELLOW, border_radius=10):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.color = color
        self.text_color = text_color
        self.border_color = border_color
        self.border_radius = border_radius
        self.font = pygame.font.Font("arcadeclassic.ttf", 28)

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, border_radius=self.border_radius)
        pygame.draw.rect(surface, self.border_color, self.rect, 2, border_radius=self.border_radius)
        text_surface = self.font.render(self.text, True, self.text_color)
        text_rect = text_surface.get_rect(center=self.rect.center)
        surface.blit(text_surface, text_rect)

    def is_clicked(self, pos):
        return self.rect.collidepoint(pos)

class Snake:
    def __init__(self):
        self.body = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
        self.direction = RIGHT
        self.last_direction = RIGHT
        self.grow = 0

    def move(self):
        head = self.body[0]
        new_head = (head[0] + self.direction[0], head[1] + self.direction[1])
        self.body.insert(0, new_head)
        if self.grow > 0:
            self.grow -= 1
        else:
            self.body.pop()
        self.last_direction = self.direction

    def change_direction(self, new_direction):
        if (new_direction[0] * -1, new_direction[1] * -1) != self.last_direction:
            self.direction = new_direction

class Food:
    def __init__(self, snake, wall_positions):
        self.wall_positions = wall_positions
        self.position = self.random_position(snake)
        self.is_bonus = False
        self.spawn_time = 0
        self.duration = 6000 

    def random_position(self, snake):
        while True:
            position = (random.randint(1, GRID_WIDTH - 2), 
                        random.randint(1, GRID_HEIGHT - 2))
            if position not in snake.body and position not in self.wall_positions:
                return position

    def spawn_bonus(self, snake):
        self.is_bonus = True
        self.position = self.random_position(snake)
        self.spawn_time = pygame.time.get_ticks()

    def update(self, current_time):
        if self.is_bonus and current_time - self.spawn_time > self.duration:
            self.is_bonus = False

def draw_grid(screen):
    for x in range(0, WIDTH, GRID_SIZE):
        pygame.draw.line(screen, WHITE, (x, 0), (x, HEIGHT))
    for y in range(0, HEIGHT, GRID_SIZE):
        pygame.draw.line(screen, WHITE, (0, y), (WIDTH, y))

def draw_snake(screen, snake):
    for i, segment in enumerate(snake.body):
        x, y = segment[0] * GRID_SIZE, segment[1] * GRID_SIZE
        
        if i == 0: 
            pygame.draw.ellipse(screen, SNAKE_GREEN, (x, y, GRID_SIZE, GRID_SIZE))
            
            eye_radius = GRID_SIZE // 5
            left_eye = (x + GRID_SIZE // 4, y + GRID_SIZE // 3)
            right_eye = (x + 3 * GRID_SIZE // 4, y + GRID_SIZE // 3)
            pygame.draw.circle(screen, WHITE, left_eye, eye_radius)
            pygame.draw.circle(screen, WHITE, right_eye, eye_radius)
            pygame.draw.circle(screen, BLACK, left_eye, eye_radius // 2)
            pygame.draw.circle(screen, BLACK, right_eye, eye_radius // 2)
        else:
            points = [
                (x, y + GRID_SIZE // 2),
                (x + GRID_SIZE // 4, y),
                (x + GRID_SIZE // 2, y + GRID_SIZE // 2),
                (x + 3 * GRID_SIZE // 4, y),
                (x + GRID_SIZE, y + GRID_SIZE // 2),
                (x + 3 * GRID_SIZE // 4, y + GRID_SIZE),
                (x + GRID_SIZE // 2, y + GRID_SIZE // 2),
                (x + GRID_SIZE // 4, y + GRID_SIZE)
            ]
            pygame.draw.polygon(screen, SNAKE_GREEN, points)
            pygame.draw.polygon(screen, SNAKE_DARK_GREEN, points, 2)

def draw_food(screen, food):
    if food.is_bonus:
        screen.blit(bonus_food_img, (food.position[0] * GRID_SIZE, food.position[1] * GRID_SIZE))
    else:
        screen.blit(food_img, (food.position[0] * GRID_SIZE, food.position[1] * GRID_SIZE))

def draw_walls(screen):
    for wall in WALL_POSITIONS:
        pygame.draw.rect(screen, RED, (wall[0] * GRID_SIZE, wall[1] * GRID_SIZE, GRID_SIZE, GRID_SIZE))

def save_highscore(user_id, game_mode, score, supabase):
    try:
        result = supabase.table('highscores').insert({
            'user_id': user_id,
            'game': game_mode,
            'score': score
        }).execute()
        print(f"Highscore saved: {result}")
    except Exception as e:
        print(f"Error saving highscore: {str(e)}")

def game_over(screen, score, difficulty, user_id, supabase):
    bg_music.stop()
    gameover_sound.play()
    font = pygame.font.Font("arcadeclassic.ttf", 38)
    text = font.render(f"Game Over! Score: {score}", True, YELLOW)
    screen.blit(text, (WIDTH // 2 - text.get_width() // 2, HEIGHT // 2 - text.get_height() // 2 - 50))

    play_again_button = PlayAgainButton(WIDTH // 2 - 150, HEIGHT // 2 + 50, 300, 50)
    main_menu_button = ReturnMenuButton(WIDTH // 2 - 150, HEIGHT // 2 + 120, 300, 50)

    play_again_button.draw(screen)
    main_menu_button.draw(screen)
    pygame.display.flip()

    game_mode = "Snake fara pereti" if difficulty == "beginner" else "Snake cu pereti"
    save_highscore(user_id, game_mode, score, supabase)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "quit"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if play_again_button.is_clicked(event.pos):
                    return "play_again"
                elif main_menu_button.is_clicked(event.pos):
                    return "main_menu"

def select_difficulty():
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Snake Game - Select Difficulty")
    
    beginner_button = Button(WIDTH // 2 - 150, HEIGHT // 2 - 60, 300, 50, "Fara pereti", BLACK, WHITE, YELLOW, 10)
    advanced_button = Button(WIDTH // 2 - 150, HEIGHT // 2 + 10, 300, 50, "Cu pereti", BLACK, WHITE, YELLOW, 10)

    while True:
        screen.fill(BLACK)
        beginner_button.draw(screen)
        advanced_button.draw(screen)
        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return None
            if event.type == pygame.MOUSEBUTTONDOWN:
                if beginner_button.is_clicked(event.pos):
                    return "beginner"
                elif advanced_button.is_clicked(event.pos):
                    return "advanced"

def run_snake(user_id, supabase):
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Snake Game")
    clock = pygame.time.Clock()

    while True:
        difficulty = select_difficulty()
        if difficulty is None:
            return

        snake = Snake()
        food = Food(snake, WALL_POSITIONS if difficulty == "advanced" else [])
        score = 0
        speed = 10  
        speed_increase = 2  

        bg_music.play(-1)  

        last_bonus_spawn = pygame.time.get_ticks()
        bonus_spawn_interval = 30000  

        running = True
        while running:
            current_time = pygame.time.get_ticks()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    bg_music.stop()
                    return
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP and snake.last_direction != DOWN:
                        snake.change_direction(UP)
                    elif event.key == pygame.K_DOWN and snake.last_direction != UP:
                        snake.change_direction(DOWN)
                    elif event.key == pygame.K_LEFT and snake.last_direction != RIGHT:
                        snake.change_direction(LEFT)
                    elif event.key == pygame.K_RIGHT and snake.last_direction != LEFT:
                        snake.change_direction(RIGHT)

            snake.move()

            if difficulty == "advanced":
                if snake.body[0] in WALL_POSITIONS:
                    result = game_over(screen, score, difficulty, user_id, supabase)
                    if result == "quit":
                        return
                    elif result == "play_again":
                        break
                    elif result == "main_menu":
                        return  
            else: 
                snake.body[0] = (snake.body[0][0] % GRID_WIDTH, snake.body[0][1] % GRID_HEIGHT)

            if snake.body[0] == food.position:
                eat_sound.play()
                if food.is_bonus:
                    snake.grow += 3
                    score += 3
                    food.is_bonus = False
                else:
                    snake.grow += 1
                    score += 1
                food.position = food.random_position(snake)
                
                if score % 10 == 0:
                    speed += speed_increase

            if snake.body[0] in snake.body[1:]:
                result = game_over(screen, score, difficulty, user_id, supabase)
                if result == "quit":
                    return
                elif result == "play_again":
                    break
                elif result == "main_menu":
                    return 

            if current_time - last_bonus_spawn > bonus_spawn_interval and not food.is_bonus:
                food.spawn_bonus(snake)
                last_bonus_spawn = current_time

            food.update(current_time)

            screen.fill(BLACK)
            draw_grid(screen)
            if difficulty == "advanced":
                draw_walls(screen)
            draw_snake(screen, snake)
            draw_food(screen, food)

            font = pygame.font.Font("arcadeclassic.ttf", 28)
            score_text = font.render(f"Scor: {score}", True, YELLOW)
            score_rect = score_text.get_rect(topleft=(10, 10))
            pygame.draw.rect(screen, BLACK, score_rect)  
            screen.blit(score_text, (10, 10))

            pygame.display.flip()
            clock.tick(speed) 

if __name__ == "__main__":
    run_snake()
    pygame.quit()